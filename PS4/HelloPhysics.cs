using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.DebugView;

namespace PS4
{
    public class HelloPhysics : Microsoft.Xna.Framework.Game
    {
        private SpriteFont announcePont;
        private Vector2 anouncePos;

        private bool gameOver = false;
        GraphicsDeviceManager graphics;
        Random rnd = new Random();

        private float timer;
        private float timer2;
        private bool startOfTheGame=true;

        private float numOfBricks = 0.6f;
        private float velOfBricks = 0.5f;
        private bool hurryUp = false;


        Fixture ball;
        int score = 0;

        SpriteBatch spriteBatch;

        // Physics
        private World world;
        private float dt = 0.001f;
        private const float G = 9.81f;
        private Vector2 gravity = new Vector2(0.0f, -G);

        private bool paused = false;

        private List<Fixture> circles = new List<Fixture>();
        List<Fixture> platforms = new List<Fixture>();


       
        // Camera
        private Camera2D camera;

        // Input
        private InputHandler input;

        // Debug view
        DebugViewXNA debugView;
        
        public HelloPhysics()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.PreferMultiSampling = true;
            IsFixedTimeStep = true;
            
            graphics.PreferredBackBufferWidth = 600;
            graphics.PreferredBackBufferHeight = 980;
                        
            Content.RootDirectory = "Content";

            input = new InputHandler(this);
            Components.Add(input);

            world = new World(gravity);
            debugView = new DebugViewXNA(world);

        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            camera = new Camera2D(GraphicsDevice);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            anouncePos = new Vector2(150,300);

            announcePont = Content.Load<SpriteFont>(@"Arial");

#if true
            ball = FixtureFactory.AttachCircle(1.5f,200.0f, new Body(world, new Vector2(0.0f, 20.0f)));
            //c1.Body.AngularVelocity = 1;
            ball.Body.BodyType = BodyType.Dynamic;
            ball.CollisionCategories = Category.Cat1;
            ball.Restitution = 0.8f;
            circles.Add(ball);
            List<Fixture> leftFixtures = new List<Fixture>();

#endif
            
            debugView.LoadContent(GraphicsDevice, Content);
            debugView.DefaultShapeColor = Color.White;
            debugView.SleepingShapeColor = Color.White;

            debugView.RemoveFlags(DebugViewFlags.Joint);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            HandleKeyboard(gameTime);
            debugView.DrawString(10,20,"Score: "+score.ToString());



            // Advance the physics simulation
            if (!paused)
            {
                world.Step(Math.Min((float)gameTime.ElapsedGameTime.TotalMilliseconds * dt, (1f / 30f)));
                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                timer2 += (float)gameTime.ElapsedGameTime.TotalSeconds;

            }
            if ((score >= 15)&&(score<20))
            {
                numOfBricks = 0.48f;
                velOfBricks = 0.35f;

                hurryUp = true;
            }
            if (score>=20)
            {
                hurryUp = false;
            }
            if (timer > numOfBricks)
            {
                Fixture temp = FixtureFactory.AttachRectangle(10.0f, 2.0f, 1.0f, new Vector2((float)rnd.Next(-15, 15), 30), new Body(world));
                temp.CollisionCategories = Category.All;
                platforms.Add(temp);
                timer = 0;
                score++;

            }
            if (timer > velOfBricks)
            {

                foreach (Fixture f in platforms.ToArray())
                {
                    f.Body.Position = new Vector2(f.Body.Position.X, f.Body.Position.Y - 1);
                    //if (c1.Body.Position == f.Body.Position)
                    //    leftFixtures.Remove(f);

                }
            }
            if (timer2 > 3)
                startOfTheGame = false;
            // If the player loses
            if (ball.Body.Position.Y < -23)
            {
                debugView.DrawString(new Vector2(0, 0), "You Lose");
                paused = true;
                gameOver = true;

            }

            //if (c1.CollidesWith == Category.Cat9)
            //    {
            //        Exit();
            //    }


            base.Update(gameTime);

        }



        private void HandleKeyboard(GameTime gameTime)
        {

         
            if (input.KeyboardHandler.IsKeyDown(Keys.Up))
            {
                circles[0].Body.ApplyLinearImpulse(new Vector2(0, 1000f));


            }
            if (input.KeyboardHandler.IsKeyDown(Keys.Left))
            {
                circles[0].Body.ApplyLinearImpulse(new Vector2(-1000, 0));


            }
            if (input.KeyboardHandler.IsKeyDown(Keys.Right))
            {
                circles[0].Body.ApplyLinearImpulse(new Vector2(1000, 0));


            }
            if (input.KeyboardHandler.IsKeyDown(Keys.Down))
            {
                circles[0].Body.Position = new Vector2(circles[0].Body.Position.X, circles[0].Body.Position.Y - 1);

            }
            if (!paused && input.KeyboardHandler.WasKeyPressed(Keys.Space))
            {
                Vector2 impulse = new Vector2(0.5f, 0.5f);
                circles[0].Body.ApplyLinearImpulse(ref impulse);
            }

            if (paused && input.KeyboardHandler.IsHoldingKey(Keys.Left))
                world.Step(Math.Min((float)gameTime.ElapsedGameTime.TotalMilliseconds * dt, (1f / 30f)));

            if (input.KeyboardHandler.WasKeyPressed(Keys.F1))
                ToggleFlag(DebugViewFlags.Shape);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F2))
                ToggleFlag(DebugViewFlags.DebugPanel);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F3))
                ToggleFlag(DebugViewFlags.PerformanceGraph);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F4))
                ToggleFlag(DebugViewFlags.AABB);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F5))
                ToggleFlag(DebugViewFlags.CenterOfMass);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F6))
                ToggleFlag(DebugViewFlags.Joint);
            if (input.KeyboardHandler.WasKeyPressed(Keys.F7))
            {
                ToggleFlag(DebugViewFlags.ContactPoints);
                ToggleFlag(DebugViewFlags.ContactNormals);
            }
            if (input.KeyboardHandler.WasKeyPressed(Keys.F8))
                ToggleFlag(DebugViewFlags.PolygonPoints);
            if (input.KeyboardHandler.WasKeyPressed(Keys.P))
                paused = !paused;
            if (input.KeyboardHandler.WasKeyPressed(Keys.G))
            {
                if (world.Gravity.Equals(Vector2.Zero))
                    world.Gravity = new Vector2(0.0f, -G);
                else
                    world.Gravity = Vector2.Zero;
            }
        }

        private void ToggleFlag(DebugViewFlags flag)
        {
            if ((debugView.Flags & flag) == flag)
                debugView.RemoveFlags(flag);
            else
                debugView.AppendFlags(flag);
        }

        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            GraphicsDevice.Clear(Color.CornflowerBlue);
            if (startOfTheGame)
            {
                spriteBatch.DrawString(announcePont, "Dont touch the floor!", new Vector2(10,150), Color.White);

            }
            if (hurryUp)
            {
                spriteBatch.DrawString(announcePont, "Hurry up!", new Vector2(150, 150), Color.White);

            }

            if (gameOver)
            spriteBatch.DrawString(announcePont, "You lose!", anouncePos, Color.White);


            debugView.RenderDebugData(ref Camera2D.Projection, ref Camera2D.View);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
